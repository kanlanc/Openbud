# Openbud
When an open source project starts, it is normally accompanied by a single person and only when that person does a lot of work and gets the project up and running, do other open source contributors come to contribute. This project is to help these early projects to find other interested contributors to help when the project is in its early stage itself, also when a different contributor proposes similar kind of project, he will be mapped to other projects that might be similar, so that he can join them.



## Things to Do

[ ]-In the projects page make a different component to contain all the projects and get the number of cards based on the number of projects

[ ]-Protect the route of adding a project

[ ]-Connect the tags to the microservice that returns the tags

[ ]-Make a navbar 

[ ]-Design the UserDashboard and make a drawer accordingly

[ ]-Make a drawer for navigating the website and in it 

    [1]-When the user is logged in the links will be:- 

        Search for a project
        Add a project
        My Dashboard
        Logout

    [2]-When the user is not Logged in the links will be:- 

        Search for a project
        Login
        Register
        


[ ]-Make a Logout route in the frontend and backend

[ ]-Change the design of the cards according to the layout in the google Scholarship showcase app

[ ]-Make a cloudinary that is a way for uploading files(images of the project and the contributors)  in the frontend and change the model in the backend

[ ]-Make routes for updating the project and deleting a project

[ ]-Make routes for updating the user and deleting a profiles

[ ]-Make another page for complaints section for the website

[ ]-Make a comments section for each project and make it such that the owner will recieve a notification whenever a user comments




